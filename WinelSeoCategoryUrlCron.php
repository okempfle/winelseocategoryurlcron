<?php
/**
 * Created by PhpStorm.
 * User: Oli
 * Date: 11.09.2018
 * Time: 09:30
 */

namespace WinelSeoCategoryUrlCron;

use Shopware\Components\Plugin;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UninstallContext;

class WinelSeoCategoryUrlCron extends Plugin
{
    public static function getSubscribedEvents() {
        return [
            'Shopware_CronJob_WinelSeoCategoryUrlCron' => 'WinelSeoCategoryUrlRun'
        ];
    }

    public function install(InstallContext $context) {
        $this->addCron();

    }

    public function uninstall(UninstallContext $context) {
        $this->removeCron();
    }

    public function addCron() {
        $connection = $this->container->get('dbal_connection');
        $connection->insert(
            's_crontab', [
            'name' => 'WinelSeoCategoryUrlCron',
            'action' => 'WinelSeoCategoryUrlCron',
            'next' => new \DateTime(),
            'start' => null,
            '`interval`' => '86400',
            'active' => 1,
            'end' => new \DateTime(),
            'pluginID' => null
        ], [
                'next' => 'datetime',
                'end' => 'datetime',
            ]
        );
    }
    public function removeCron() {
        $this->container->get('dbal_connection')->executeQuery('DELETE FROM s_crontab WHERE `name` = ?', [
            'WinelSeoCategoryUrlCron'
        ]);
    }


    public function WinelSeoCategoryUrlRun(\Shopware_Components_Cron_CronJob $job) {
        $shops = $this->getShops();
        foreach ($shops as $shop) {
            $shop_id = $shop['id'];
            $main_category_id=$shop['category_id'];
            $categories = $this->getCategoryIds($main_category_id);
            foreach ($categories as $category) {
                $categoryId=$category['id'];
               // $categoryName=$category['description'];
                $categoryPath = $this->getCategoryPath($categoryId);
                $exists=$this->checkSeoUrl($shop_id,$categoryPath);
                if(!$exists){
                    $orgPath='sViewport=cat&sCategory='.$categoryId;
                    $this->container->get('dbal_connection')->executeQuery('insert into s_core_rewrite_urls (org_path, path,main, subshopID) VALUES (?,?,?,?)', [$orgPath , $categoryPath,1, $shop_id]);
                }
            }
        }
        $cacheManager = Shopware()->Container()->get('shopware.cache_manager');
        $cacheManager->clearHttpCache();
    }

    private function getShops(){
            $shops = Shopware()->Db()->fetchAll(
                'SELECT name, id,category_id 
            FROM s_core_shops where active=?
                    ', [1]
            );
          return $shops;
    }

    private function getCategoryIds($parentId) {
        $categories = Shopware()->Db()->fetchAll(
            'SELECT id , description
            FROM s_categories where path like ?
                    ', ['%|' . $parentId . '|%']
        );

        return $categories;
    }

    private function checkSeoUrl($subshop_id,$categoryPath) {
        $check=false;

        $rewriteUrls = Shopware()->Db()->fetchAll(
            'SELECT id 
            FROM s_core_rewrite_urls where path like ? and subshopID=?
                    ', [$categoryPath,$subshop_id]
        );


        if (count($rewriteUrls) > 0) {
            $check= true;
        }
        return $check;

    }
    private function getCategorypath($cat_id) {
        $path='';
        $categories = Shopware()->Db()->fetchAll(
            'SELECT path , description
            FROM s_categories where id=?
                    ', [$cat_id]
        );

        foreach ($categories as $categoriy) {
            $description=$categoriy['description'];
            $catPath=$categoriy['path'];

            $path_array=explode('|',$catPath);
            $path_array=array_reverse($path_array);
            foreach($path_array as $pcat_id){
                $UpCategories = Shopware()->Db()->fetchAll(
                    'SELECT path , description
            FROM s_categories where id=?
                    ', [$pcat_id]
                );
                foreach ($UpCategories as $UpCategory) {
                    $desc=$UpCategory['description'];
                    $upCatPath=$UpCategory['path'];
                    if($upCatPath!== NULL){
                        $desc=$this->prepareNames($desc);
                        $path.=$desc.'/';
                    }
                }
            }
            $path.=$this->prepareNames($description);
        }
        $path.='/';
        $path=strtolower($path);

        return $path;
    }

    private function prepareNames($desc) {

        $desc=  strtolower($desc);

        $desc= str_replace('Ä','ae',$desc);
        $desc= str_replace('ä','ae',$desc);
        $desc= str_replace('ö','oe',$desc);
        $desc= str_replace('Ö','oe',$desc);
        $desc= str_replace('ü','ue',$desc);
        $desc= str_replace('Ü','ue',$desc);
        $desc= str_replace('ß','ss',$desc);
        $desc= str_replace('é','e',$desc);
        $desc= str_replace('è','e',$desc);
        $desc= str_replace('à','a',$desc);
        $desc= str_replace('á','a',$desc);
        $desc= str_replace('ó','o',$desc);
        $desc= str_replace('ô','o',$desc);
        $desc= str_replace('ó','o',$desc);
        $desc= str_replace('í','i',$desc);
        $desc= str_replace('/','-',$desc);
        $desc= str_replace('.','',$desc);
        $desc= str_replace(' ','-',$desc);
        $desc= str_replace('&','',$desc);
        $desc= str_replace('--','-',$desc);
        $desc= str_replace('--','-',$desc);
        $desc= str_replace('-&-','-',$desc);

        return $desc;

    }

}